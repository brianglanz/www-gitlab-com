---
layout: markdown_page
title: "Self-managed Scalability Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value        |
|-----------------|--------------|
| Date Created    | May 21, 2019 |
| Target End Date | TBD          |
| Slack           | [#wg_sm-scalability](https://gitlab.slack.com/messages/CJBEAQ589) (only accessible from within the company) |
| Google Doc      | [Self-managed Scalability Working Group Agenda](https://docs.google.com/document/d/1H9ENjGO5vNI1e0j3lm2e6zeK8F8o8H-69M3V7m3uYt8/edit) (only accessible from within the company) |

## Business Goal

Ensure all new customers are set up in a standardized environment that will scales with their needs. Migrate existing customers to an appropriate reference environment.

## Exit Criteria

* Documented 10k user reference architecture
* Implemented 10k reference environment with stress testing
* Environmental healthcheck implemented in GitLab project
* Inventory of self-managed customers with scores of their environment
* Self-monitoring with Grafana turned on for all large customers
* Migration strategy and messaging for environmental changes for 10 out-of-spec customers
* List of additional needed reference architectures

## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Facilitator           | Eric Johnson          | VP of Engineering              |
| Support Lead          | Drew Blessing         | Staff Support Engineer         |
| CS Lead               | TBD                   | TBD                            |
| Quality Lead          | Tanya Pazitny         | Interim Quality Manager        |
| Development Lead      | Ben Kochie            | Staff BE Engineer, Monitor     |
| PM Lead               | Kenny Johnston        | Director of Product, Ops       |
| Member                | Tom Cooney            | Director of Support            |
| Member                | Chun Du               | Director of Engineering        |
| Executive Stakeholder | David Sakamoto        | VP of Customer Success         |
