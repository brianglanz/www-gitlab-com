---
layout: markdown_page
title: "Category Vision - Conan Repository"
---

- TOC
{:toc}

## Conan Repository
As part of our overall vision for packaging at GitLab, we want to provide a single interface for managing dependencies, registries, and package repositories. C/C++ developers are an important group of users who need these capabilities and [Conan](https://conan.io) is an open-source solution that is already available and has been requested by GitLab users. By integrating with Conan, GitLab will enable C/C++ developers to build, publish and share all of their dependencies, all from within GitLab. 

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Conan%20Repository)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1323) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

The first item on our agenda is to implement [gitlab-ee#8248](https://gitlab.com/gitlab-org/gitlab-ee/issues/8248), the MVC for the GitLab Conan Repository,
which will provide a base feature for users to start integrating with and providing
feedback.

## Maturity Plan
This category is currently at the "Planned" maturity level, and
our next maturity target is minimal (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).
Key deliverables to achieve this are:

- [MVC for Conan C/C++ Repository](https://gitlab.com/gitlab-org/gitlab-ee/issues/8248)

## Competitive Landscape

- [JFrog Artifactory](https://www.jfrog.com/confluence/display/RTF/Conan+Repositories)
- [Sonatype Nexus](https://github.com/sonatype-nexus-community/nexus-repository-conan) (through community plugin)
- [GitHub](https://github.blog/2019-05-10-introducing-github-package-registry/) does not yet support Conan

## Top Customer Success/Sales Issue(s)

Because this category is still planned, we do not yet have CS/Sales issues apart
from the request to implement the MVC.

## Top Customer Issue(s)

Because this category is still planned, we do not yet have CS/Sales issues apart
from the request to implement the MVC.

## Top Internal Customer Issue(s)

For GitLab is this is not applicable because we do not develop in C/C++.

## Top Vision Item(s)

Our vision begins with implementing the [gitlab-ee#8248](https://gitlab.com/gitlab-org/gitlab-ee/issues/8248),
which will allow us to kick off support. Beyond the initial MVC implementation, our
focus will be on improving consistency and core features across all of our repository
types rather than providing C/C++ specific features. This may change as we begin
to get customer feedback from real-world users of the new repository.