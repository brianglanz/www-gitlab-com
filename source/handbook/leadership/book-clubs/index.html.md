---
layout: markdown_page
title: "Book clubs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

From time to time, we run internal book clubs on a [book from our
leadership page](/handbook/leadership/#books). To propose a new book
club, [create an
issue](https://gitlab.com/gitlab-com/book-clubs/issues/new) in the [book
clubs project](https://gitlab.com/gitlab-com/book-clubs).

## High Output Management

* Dates: 2019-06-03 to 2019-07-15 (every two weeks)
* Time: 7:30 Pacific Time (one hour before the company call)
* [Zoom](https://gitlab.zoom.us/j/544984602)
* [Meeting agenda](https://docs.google.com/document/d/1gQZahLk2LYDbYAb4TeYqNOQbF8f6MrCaATLyPTzRONY/edit)
* [Discussion issue](https://gitlab.com/gitlab-com/book-clubs/issues/3)

## Crucial Conversations

This book club was internal-only, intended specifically for engineering
managers.

* Dates: 2018-10-01 to 2018-11-05
* [Notes](https://docs.google.com/document/d/1lY-v9zRdSxtVKu-yh3U7oz5kNen2YZE_m5OeNF0QNHM/edit)
* [Recordings](https://drive.google.com/drive/u/0/folders/1lqtdN4eWLG0RxqV8KSsnp8__P__Bff-2)
