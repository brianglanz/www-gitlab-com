---
layout: markdown_page
title: "SDM.2.01 - Whitepapers Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SDM.2.01 - Whitepapers

## Control Statement

GitLab publishes whitepapers to its public website that describe the purpose, design, and boundaries of the system and system components.

## Context

Whitepapers and other public resources help customers and partners better understand GitLab's systems. This control also helps us achieve our value of [transparency](https://about.gitlab.com/handbook/engineering/infrastructure/production-architecture/#network-architecture).

## Scope

This control applies to all departments and teams at GitLab.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SDM.2.01_whitepapers.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SDM.2.01_whitepapers.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SDM.2.01_whitepapers.md).

## Framework Mapping

* SOC2 CC
  * CC2.3
