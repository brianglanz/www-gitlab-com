---
layout: markdown_page
title: Support Engineer Ops
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

Support Ops are the tools that are created by the support team to automate
tasks or improve our workflows.

## Premium Breach Notifications
{: #premium-breach-notifications}

The premium breach notifications is a webhook that is triggered by a ZenDesk
automation titled `Premium one hour breach tag`. The trigger looks for any
high premium tagged tickets that have less than 2 hours left on the breach clock.
The automation [runs at the top of every hour](https://support.zendesk.com/hc/en-us/articles/203662236-About-automations-and-how-they-work)
and due to this limitation, it does not run at the exact hour the ticket is less than 2 hour from breaching.

Once the ticket is updated, a trigger sends a webhook to slack which is
configured on the slack side.

The webhook is formatted in json:

```json
{
  "attachments":[
    {
        "color":"#FAB617",
        "author_name": "Ticket #{{ticket.id}} - {{ticket.title}}",
        "author_link": "{{ticket.url}}"
    }
  ]
}
```


## #zd-self-managed-feed and #zd-gitlab-com-feed
{: #zd-self-managed-feed}

The `#zd-self-managed-feed` channel is updated by a webhook that shows any self-managed ticket that is updated.
It's different from the ZenDesk Slack integration because it shows organization names.

It only updates created and updated tickets.

The `#zd-self-managed-feed` channel does not display tickets addressed to GitLab.com or
GitHost.io. The `#zd-gitlab-com-feed` does not display EE tickets.

The webhook is formatted in json:

```json
{
  "attachments": [
  {
     "fallback": "",
     "color": "#4AACD8",
     "pretext": "{{ticket.status}} ticket updated by {{current_user.name}}:
        <{{ticket.link}}|Ticket #{{ticket.id}}>",
        "title": "{{ticket.title}}",
        "author_name": "{{ticket.organization.name}}",
        "title_link": "{{ticket.link}}",
        "text": "{{ticket.latest_comment}}",
        "fields": [
          {
          "short":
          true
          }
     ],
     "footer": "ZenDesk",
     "footer_icon":
     "https://slack-imgs.com/?c=1&o1=wi32.he32.si&url=https%3A%2F%2Fa.slack-edge.com%2F436da%2Fimg%2Funfurl_icons%2Fzendesk.png"
  }
]
}
```

## Feedback
{: #feedback}

Users are sent a satisfaction survey after Zendesk tickets are `closed`. When a satsifaction survey is completed with comment, that feedback is pushed to an issue in the [Customer Feedback project](https://gitlab.com/gitlab-com/support/feedback/issues) for further review. Managers are expected to action feedback with Bad ratings, though the support team and GitLab team-members are [encouraged to collaborate](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1623) on these issues.

The webhook is formatted in `json`:

```
{% assign form = {{ticket.ticket_form}} %}

{% if form contains 'Accounts' %}
   {% assign form = 'form::Accounts Receivable' %}
 {% elsif form contains 'License' %}
   {% assign form = 'form::Upgrades & Renewals' %}
 {% elsif form contains 'GitLab.com' %}
   {% assign form = 'form::GitLab.com' %}
 {% elsif form contains 'Self' %}
   {% assign form = 'form::Self-managed' %}
 {% else %}
 {% assign form = '' %}
{% endif %}

{% assign rating = {{satisfaction.current_rating}} %}
{% if rating contains 'Good' %}
{
	"title": "{{ticket.title}}",
	"labels": "satisfaction::Good,{{form}}",
	"description": "{{satisfaction.current_comment}}\n\n**Ticket:** [#{{ticket.id}}](https://{{ticket.url}})"
}
{% else %}
{
	"title": "{{ticket.title}}",
	"labels": "satisfaction::Bad,{{form}}",
	"description": "{{satisfaction.current_comment}}\n\n**Ticket:** [#{{ticket.id}}](https://{{ticket.url}})\n\n/cc @gitlab-com/support/managers"
}
{% endif %}
```

- [Zendesk trigger](https://gitlab.zendesk.com/agent/admin/triggers/360088483733)
- [Zendesk HTTP target](https://gitlab.zendesk.com/targets/360001281193/edit)
- [JSON snippet](https://gitlab.com/gitlab-com/support/feedback/snippets/1860115)

## Other tools
{: #other-tools}

[Supportbot](https://gitlab.com/gl-support/gitlab-support-bot/tree/master/) : Bot for getting ZenDesk tickets.
