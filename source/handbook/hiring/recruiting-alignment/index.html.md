---
layout: markdown_page
title: "Recruiting Alignment"
---

## Recruiter, Coordinator and Sourcer Alignment by Department

| Department                    | Recruiter       | Coordinator     |Sourcer     |
|--------------------------|-----------------|-----------------|-----------------|
| Board of Directors          |    | April Hoffbauer | |
| Executive          |    | April Hoffbauer | Anastasia Pshegodskaya |
| Enterprise Sales, North America | Britney Sabbah-Mani   | Chantal Rollison |Kanwal Matharu        |
| Commercial Sales,	NA/EMEA/APAC | Marcus Carter    | Chantal Rollison |Kanwal Matharu        |
| Enterprise Sales, 	EMEA | Stephanie Garza    | Bernadett Gal |Kanwal Matharu        |
| Field Operations,	NA/EMEA/APAC | Britney Sabbah-Mani   | Chantal Rollison |Kanwal Matharu        |
| Customer Success, 	North America | Stephanie Garza    | Bernadett Gal |Kanwal Matharu        |
| Customer Success, 	EMEA/ APAC | Sean Delea  | Bernadett Gal |Viren Rana |
| Federal Sales/Customer Success | Stephanie Kellert   | Chantal Rollison |Viren Rana |
| Marketing,	North America | Jacie Zoerb   | Chantal Rollison |Viren Rana |
| Marketing and G&A,	EMEA | Sean Delea   | Bernadett Gal |Viren Rana |
| G&A, 	North America | Jacie Zoerb/Stephanie Garza   | Chantal Rollison/Bernadett Gal |Viren Rana/Kanwal Matharu |
| Quality                   | Rupert Douglas                                          | Kike Adio        | Zsusanna Kovacs      |
| UX                        | Rupert Douglas                                          | Kike Adio        | Zsusanna Kovacs      |
| Technical Writing         | Rupert Douglas                                          | Kike Adio        | Zsusanna Kovacs      |
| Support                   | Cyndi Walsh                                             | Emily Mowry      | Zsusanna Kovacs      |
| Security                  | Cyndi Walsh                                             | Emily Mowry      | Zsusanna Kovacs      |
| Infrastructure            | Matt Allen                                              | Emily Mowry      | Anastasia Pshegodskaya |
| Development - Dev         | Catarina Ferreira                                       | Kike Adio        | TBD (Zsuzsanna Kovacs - Interim)       |
| Development - Secure/Defend      | Liam McNally                                            | Kike Adio        | Alina Moise       |
| Development - Ops & CI/CD  | Eva Petreska                                            | Emily Mowry      | TBD (Zsuzsanna Kovacs - Interim)       |
| Development - Enablement  | Trust Ogor                                              | Kike Adio        | Alina Moise       |
| Development - Growth      | Trust Ogor                                              | Kike Adio        | Alina Moise       |
| Engineering Leadership                | Steve Pestorich                                         | Emily Mowry      |  Anastasia Pshegodskaya |
| Product Management  | Matt Allen                      | Emily Mowry |  Anastasia Pshegodskaya |
