---
layout: markdown_page
title: "Operating Metrics"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Key Monthly Review

### Purpose

For each [executive](/company/team/structure/#executives) we have a monthly call to discuss the Key Performance Indicators and OKRs of that department in order to:

1. Makes it much easier to stay up to date for everyone.
1. Be accountable to the rest of the company.
1. Understand month to month variances.
1. Understand against the plan, forecast and operating model.
1. Ensure there is tight connection between OKRs and KPIs.

Some executives will have additional calls in areas that report to them based on the number and importance of KPI/OKRs associated with the function.

### Metric

1. [KPIs](/handbook/ceo/kpis/) of that department
1. [OKRs](/company/okrs/) that are assigned to this executive.
1. Corporate metrics sheet (search in google docs for GitLab Metrics)
1. Operating Model (search in google docs for GitLab Financial Model)

### Agenda

1. Review KPIs and conclude on implications for operating model.
1. Review status of current quarter OKRs.
1. Discuss proposals for different measurement.
1. Determine if external benchmarks are required.
1. Discuss proposals for addition of new KPIs.
1. Discuss proposals for deprecation of existing KPIs.
1. Review decisions & action items.

### Timing

Meetings are monthly starting on the 10th day after month end.

### Invitees

Required invites are the executive and the CFO. Optional attendees are the rest of the e-team and anyone who has an interest in the metric.

### Meeting Format

1. The functional owner will prepare a google slide presentation with the content to be reviewed.
1. The finance business partner assigned to the functional area will meet with the owner at least one week in advance and ensure that follow-ups from last meeting have been completed and that data to be presented has proper definitions and is derived from a Single Source of Truth.
1. The title of every slide should be the key takeaway
1. A label on the slide should convey whether the metric result is "on-track" (green), "needs improvement" (yellow), or is an "urgent concern" (red).
1. A google doc will also be linked from the calendar invite for participants to log questions or comments for discussion, and to any additional track decisions & action items.
1. Wherever possible the metric being reviewed should be compared to Plan, OKR target, KPI target, or industry benchmark.
1. The functional owner is expected to present a summary of highlights which should not last more than three minutes. A pre-recorded video can be an efficient way to do this.
1. The functional owner is responsible for preparing the document 24 hours advance of the meeting. The owner should update the meeting invite and send to all guests so they know the materials are ready for review.
1. A [blank template](https://docs.google.com/presentation/d/1lfQMEdSDc_jhZOdQ-TyoL6YQNg5Wo7s3F3m8Zi9NczI/edit) still needs labels

### Future

We want to get from Google Sheets to reviewing a live dashboard.

<div class="alert alert-purple center">
  <p class="purple center" style="font-size: 34px ; text-align: center ; margin: auto">
    <strong>We are actively working to move away from the methodology of maintaining one Operating Metrics page.</strong>
  </p>
</div>

**Please do not update this section. Instead, please define a metric in-place, where it would make the most sense for that metrics to live in the handbook. For example, the Average Days to Hire metrics should live within the recruiting section of the Handbook.**

For more details, see Emilie Schario, Data Analyst, and Sid Sijbrandij, CEO, discuss the best way to organize metrics definitions in the company handbook. Progress updates can be found in [gitlab-data#1241](https://gitlab.com/gitlab-data/analytics/issues/1241).

<iframe width="560" height="315" src="https://www.youtube.com/embed/T4fQp9jtKWU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Here's another follow up discussion that includes Joe Davidson, SDR, and brings additional clarity around addressing the ownership question.

<iframe width="560" height="315" src="https://www.youtube.com/embed/8PZeorUJEbE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Defined Terms for Balance Sheet KPIs and Financial Reporting 

### Calculated Billings
Calculated billings is defined as revenue plus the sequential change in total deferred revenue as presented on the balance sheet.

We do not believe that calculated billings provides a meaningful indicator of financial performance as billings can be impacted by timing volatility of renewals, co-terming upgrades and multi year prepayment of subscriptions.

### Capital Consumption
TCV less Total Operating Expenses.
This metric tracks net cash consumed excluding changes in working capital (i.e. burn due to balance sheet growth).
Since the growth in receivables can be financed with using cheap debt instead of equity is a better measure of capital efficiency than cash burn.

### Cash
Defined as cash in the bank.  Also counts short term securities that are readily convertable into cash within the next 90 days.

### Cash Burn, Average Cash Burn and Runway
The change in cash balance from period to period excluding equity or debt financing. Average cash burn is measured over the prior three months. Runway is defined as the number of months based on cash balance plus available credit divided by average cash burn. Our target is that this metric is always greater than 12 months.

### Credit
Lost or lowered contract value that occurs before a subscription renewal or subscription cancellation

### Days Sales Outstanding (DSO)
Average Accounts Receivable balance over prior 3 months divided by Total Contract Value (TCV) bookings over the same period multipied by 90 that provides an average number of days that customers pay their invoices.  Link to a good [definition](https://www.investopedia.com/terms/d/dso.asp)  and [Industry guidance](https://www.opexengine.com/software-industry-revenue-growth-accelerating-and-hiring-expected-to-jump-according-to-new-siiaopexengine-report/) suggests the median DSO for SAAS companies is 76 days. Our target at GitLab is 45 days.

### Line of Credit (LOC) Available
The amount of contractually committee line of credit extended by the bank that is not in default status.

### Free Cash Flow (FCF)
{: #fcf}
Cash flow from operations as defined by GAAP less Capital Expenditures.

### Gross Burn Rate
Total operating expenses plus capital expenditures.

### Non GAAP Revenue (Ratable Recognition)
The amount of subscription revenue recognized using ratable accounting treatment as calculated by the subscription amount divided equally over the subscription term. Note that other GAAP adjustments such as non-delivery, performance obligations are not accounted for in this metric.


## Marketing

### Cost per MQL
Marketing expense divided by the number of MQLs

### Marketing efficiency ratio
IACV / marketing spend

## Product

Operating and product metrics for self-managed and GitLab.com instances can be found [here](https://about.gitlab.com/handbook/product/metrics).

## Sales

### Average Sales Price (ASP)
IACV per won deal. This metric can be reported against various dimensions (e.g. ASP by customer segment, cohort, sales channel, territory, etc.)

### Contract Value

#### Annual Contract Value (ACV)
Current Period subscription bookings which will result in revenue over next 12 months. For multiple year deals with contracted ramps, the ACV will be the average annual booking per year.

#### Incremental Annual Contract Value (IACV)
Value of new bookings from new and existing customers that will result in recurring revenue over the next 12 months less any credits, lost renewals, downgrades or any other decrease to annual recurring revenue. Excluded from IACV are bookings that are non-recurring such as professional services, training and non-recurring engineering fees (PCV). Also equals ACV less renewals. However, bookings related to true-up licenses, although non-recurring, are included in IACV because the source of the true-ups are additional users which will result in recurring revenue.
IACV may relate to future periods (within twelve months).

Beg ARR + IACV may not equal ending ARR due to the following reasons:
1. Timing difference due to IACV that will not start until a later period.
1. ARR will be reduced by subscriptions that have expired but which may be recorded as a reduction to IACV in a different period (either earlier or later).

#### Gross Incremental Annual Contract Value (Gross IACV)
Value of new bookings from new and existing customers that will result in recurring revenue over the next 12 months. Gross IACV includes true-ups and refunds.

#### Growth Incremental Annual Contract Value (Growth IACV)
Contract value that increases at the time of subscription renewal

#### New Incremental Annual Contract Value (New IACV)
Contract value from a new subscription customer

#### ProServe Contract Value (PCV)
{: #pcv}
Contract value that is not considered a subscription and the work is performed by the Professional Services team

#### Total Contract Value (TCV)
The total value of the contract that the customer will pay up front (i.e. within 90 days from close of deal)

### Customer Acquisition Cost (CAC)
Total Sales & Marketing Expense/Number of New Customers Acquired

### Customer Acquisition Cost (CAC) Ratio
{: #cac-ratio}
Total Sales & Marketing Expense/ACV from new customers (excludes growth from existing).  [Industry guidance](http://www.forentrepreneurs.com/2017-saas-survey-part-1/) reports that median performance is 1.15 with anything less than 1.0 being considered very good.
All bookings in period (including multiyear); bookings is equal to billings with standard payment terms.

### Downgrade
Contract value that results in a lower value than the previous contract value. Downgrade examples include seat reductions, product downgrades, discounts, and customers switching to Reseller at time of renewal.

### Field efficiency ratio
IACV / sales spend

### Licensed Users
{: #licensed-users}
Number of contracted users on active paid subscriptions. Excludes OSS, Education, Core and other non-paid users. Data source is Zuora.

### Life-Time Value (LTV)
{: #ltv}
Customer Life-Time Value = Average Revenue per Year x Gross Margin% x 1/(1-K) + GxK/(1-K)^2; K = (1-Net Churn) x (1-Discount Rate).  GitLab assumes a 10% cost of capital based on current cash usage and borrowing costs.

### Life-Time Value to Customer Acquisition Cost Ratio (LTV:CAC)
{: #ltv-to-cac-ratio}
The customer Life-Time Value to Customer Acquisition Cost ratio (LTV:CAC) measures the relationship between the lifetime value of a customer and the cost of acquiring that customer. [A good LTV to CAC ratio is considered to be > 3.0.](https://www.klipfolio.com/resources/kpi-examples/saas-metrics/customer-lifetime-value-to-customer-acquisition-ratio)

### Lost Renewal
Contract value that is lost at the time of subscription renewals. Lost Renewals examples include cancellations at or before the subscription renewal date.

### Magic Number
IACV for trailing three months / Sales and marketing Spend over trailing months -6 to months -4 (one quarter lag) (see the details of this spend, as defined in the Sales Efficiency Ratio). [Industry guidance](http://www.thesaascfo.com/calculate-saas-magic-number/) suggests a good Magic Number is > 1.0. GitLab's target is to be at 1.1.

### New ACV / New Customers
Net IACV that come from New Customers divided by the number of net closed deals in the current month.

### New ACV / New Customers by Sales Assisted
Net IACV that come from New Customers and sold by the field sales team divided by the number of net closed deals in the current month.

### Revenue per Licensed User (also known as ARPU)

ARR divided by number of [Licensed Users](https://about.gitlab.com/handbook/finance/operating-metrics/#licensed-users)

### Sales Efficiency Ratio
[IACV](/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) / sales and marketing spend. Sales and marketing spend comes from Netsuite; it is all expenses from accounts between 6000 and 6999, inclusive. [Industry guidance](http://tomtunguz.com/magic-numbers/) suggests that average performance is 0.8 with anything greater than 1.0 being considered very good. GitLab's target is greater than 1. 

### Sales Qualified Lead (SQL)
[Sales Qualified Lead](/handbook/business-ops/#customer-lifecycle)

### Rep Productivity
{: #rep-productivity}
Monthly IACV * 12 / number of native quota-carrying sales reps

### Late Stage Pipeline

The IACV of all open opportunities currently in the stages of 4-Proposal, 5-Negotiating, and 6-Awaiting Signature.

### Total Pipeline

The IACV of all open opportunities.

### Renewals (ACV)

The value of previously closed Won ACV that is up for renewal. Renewal ACV should not include ACV from Professional Services or True-Ups.

### Renewals + Existing Growth

Renewal ACV plus Growth IACV minus (Lost Renewals + Credits + Downgrades)

### Upsells/Cross sells and Extensions (IACV)

The value of the first twelve (12) months of any mid-term upgrade.

### Closed Deal - Won

An unique deal that is set to `Closed Won` in SalesForce.
