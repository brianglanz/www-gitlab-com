---
layout: markdown_page
title: "Commercial Sales - Customer Success"
---

# Commercial Sales - Customer Success Handbook
{:.no_toc}

GitLab defines Commercial Sales as the worldwide sales for the mid-market and small/medium business segment. This segment consists of two sales teams, Small Business (SMB) and Mid-Market (MM). 

* SMB = 0 - 99 employees
* Mid-Market = 100 - 1,999 employees

The Commercial Sales segment is supported by a dedicated team of Solutions Architects (SA) and Technical Account Managers (TAM). 

## On this page
{:.no_toc}

- TOC
{:toc}

## Role & Responsibilities

### Solutions Architects
* SA’s are aligned to the Commercial Sales Reps by a pooled model. Requests for an SA will be distributed between the SA's based on multiple factors including availability, applicable subject matter expertise, and workload.  
* [Solutions Architect Role Description](/job-families/sales/solutions-architect/)
* [Solutions Architect Overview](/handbook/customer-success/solutions-architects/)

### Technical Account Managers
* Technical Account Managers that support Commercial Sales are aligned by region, Americas East, Americas West, EMEA and APAC. 
* [Technical Account Manager Description](/job-families/sales/technical-account-manager/)
* [Technical Account Manager Overview](/handbook/customer-success/tam/)
