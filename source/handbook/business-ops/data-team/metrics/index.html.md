---
layout: markdown_page
title: "KPI Index"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Maintenance

This page is meant to help map a [KPI](/handbook/ceo/kpis/) to where it is defined in the handbook. The Data Team is responsible for maintaining this page.

If a KPI is not defined, create an MR to propose a definition. Ask the appropriate business stakeholder to review and merge.

## Work Prioritization
In connection with our [Q2 OKRs](/company/okrs/fy20-q2/). We have made the following classifications on which metrics will be visualized in Periscope:

- P0 - Most important and expected to be completed in Q2
- P1 - High priority with expectation to be completed in Q2
- P2 - The data currently resides in a separate data system that provides for adequate visualization of the metric. We have de-prioritized these given that the metric is already being tracked effectively. The work to be performed in Q2 will be to ensure the definitions are clear and that we are reporting the actual KPI against our plan and target.
- Out of scope for data team - These KPIs are contained in systems that are either non-existent or are not planned to be supported by the data team in Q2.
- Regardless of classification all KPIs on this page are expected to have linked definitions with known quantities for Plan and Target values.

## GitLab KPIs

GitLab KPIs are duplicates of goals of the reports further down this page.
GitLab KPIs are the most important indicators of company performance.
The [GitLab KPIs Dashboard](https://app.periscopedata.com/app/gitlab/434327/) whihch is also listed in the [Periscope directory](/handbook/business-ops/data-team/periscope-directory/).

1. [IACV](/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) vs. plan > 1 - P2
1. TCV - OpEx vs. plan > 1 - P1
1. [Sales efficiency ratio](/handbook/finance/operating-metrics/#sales-efficiency-ratio) > 1 - P1
1. Pipe generated vs. plan > 1 - P0
1. Wider community contributions per release (dependent on GitLab.com data) - P1
1. [LTV / CAC](/handbook/finance/operating-metrics/#ltv-to-cac-ratio) ratio > 4 - P1
1. Average NPS (out of scope for Q2)
1. Hires vs. plan > 0.9  (out of scope for Q2)
1. [Monthly employee turnover](/handbook/people-operations/people-operations-metrics/#turnover) - P0
1. New hire average score (out of scope for Q2)
1. Merge Requests per release per developer - P1
1. Uptime GitLab.com (out of scope for Q2 as dependent on PD)
1. Active users per hosting platform: Total, AWS, Azure, GCP, IBM, Unknown (out of scope for Q2)
1. [Support CSAT](/support/#customer-satisfaction) - P1
1. Runway > 12 months - P1
1. [MAUI](http://www.meltano.com/docs/roadmap.html#maui) (Meltano so not part of the GitLab Executive Team KPIs) > 10% WoW

## Sales KPIs

1. [IACV](/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) vs. plan > 1 - P0
1. [Field efficiency ratio](/handbook/finance/operating-metrics/#field-efficiency-ratio) > 2 - P0
1. [TCV](/handbook/finance/operating-metrics/#total-contract-value-tcv) vs. plan > 1 - P0
1. [ARR](/handbook/finance/operating-metrics/#annual-recurring-revenue-arr) YoY > 190% - P0
1. Win rate > 30% - P1
1. % of ramped reps at or above quota > 0.7 - P1
1. [Net Retention](/handbook/customer-success/vision/#measurement-and-kpis) > 2 - P0
1. [Gross Retention](/handbook/customer-success/vision/#measurement-and-kpis) > 0.9 - P0
1. Rep IACV per comp > 5 - P1
1. [ProServe](/handbook/finance/operating-metrics/#pcv) revenue vs. cost > 1.1 - P1
1. Services attach rate for strategic > 0.8 - P1
1. Self-serve sales ratio > 0.3 - P1
1. Licensed users - P1
1. [ARPU](/handbook/finance/operating-metrics/#arpu) (P0)
1. New strategic accounts - P1
1. [IACV per Rep](/handbook/finance/operating-metrics/#iacv-rep) > $1.0M - P0
1. [New hire location factor](/handbook/people-operations/global-compensation/#location-factor) < 0.72 - P0

## Marketing KPIs

1. Pipe generated vs. plan > 1 - P0
1. Pipe-to-spend > 5 - P0
1. [Marketing efficiency ratio](/handbook/finance/operating-metrics/#marketing-efficiency-ratio) > 2 - P0
1. [Sales Accepted Opportunity (SAO)](/handbook/business-ops/#criteria-for-sales-accepted-opportunity-sao) - P0
1. [LTV / CAC ratio](/handbook/finance/operating-metrics/#ltv-to-cac-ratio) > 4 - P1
1. Twitter mentions (out of scope for Q2)
1. Sessions on our marketing site (out of scope for Q2)
1. New users (P1)
1. Product Installations: Download, start of installation, success installation, created admin user, configured email, second user invited, 30 day active, updates (out of scope for Q2)
1. [Social response time](/handbook/marketing/community-relations/community-advocacy/#respond-to-every-community-question-about-gitlab-asked-online) (out of scope for Q2)
1. Meetup Participants with GitLab presentation (out of scope for Q2)
1. GitLab presentations given - P1
1. Wider community contributions per release - P1
1. Monthly Active Contributors from the wider community - P1
1. [New hire location factor](/handbook/people-operations/people-operations-metrics/#low-location-factor-reporting) < 0.72 - PO
1. Pipeline coverage: 2X for current quarter, 1X for next quarter, and .5 for 2 QTRs out. - P1

## People Operations KPIs

1. [Hires vs. plan](/handbook/hiring/metrics/#hires-vs-plan) > 0.9 (Bamboo & Sheets) -P0
1. Apply to hire days < 30 (Greenhouse) -P0
1. No offer [NPS](/handbook/finance/operating-metrics/#nps) > [4.1](https://stripe.com/atlas/guides/scaling-eng) (out of scope for Q2)
1. [Offer acceptance rate](/handbook/hiring/metrics/#offer-acceptance-rate)> 0.9 (Greenhouse) -P1
1. Average [NPS](/handbook/finance/operating-metrics/#nps) (out of scope for Q2)
1. [Average location factor](/handbook/people-operations/people-operations-metrics/#low-location-factor-reporting) (Bamboo) -P0
1. [New hire location factor](/handbook/people-operations/people-operations-metrics/#low-location-factor-reporting) < 0.72 (Bamboo) -P0
1. [12 month employee turnover](/handbook/people-operations/people-operations-metrics/#turnover) < 16% (Bamboo) -P0
1. [Voluntary employee turnover](/handbook/people-operations/people-operations-metrics/#turnover) < 10% (Bamboo) -P0
1. Candidates per vacancy (Greenhouse) -P1
1. Percentage of vacancies with active sourcing (Greenhouse) -P1
1. New hire average score (Out of scope for Q2)
1. Onboarding [NPS](/handbook/finance/operating-metrics/#nps) (out of scope for Q2)
1. Diversity lifecycle: applications, recruited, interviews, offers, acceptance, retention (Greenhouse) -P1
1. PeopleOps cost per employee (Netsuite over Bamboo) -P1
1. [Discretionary bonus](/handbook/incentives/#discretionary-bonuses) per employee per month > 0.1 (Bamboo)-P1
1. [Team Members](/handbook/people-operations/people-operations-metrics/#team-members)

## Finance KPIs

1. [IACV](/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) per [capital consumed](/handbook/finance/operating-metrics/#capital-consumption) > 2 -P0
1. [Sales efficiency](/handbook/finance/operating-metrics/#sales-efficiency-ratio) > 1.0 -P0
1. [Magic number](/handbook/finance/operating-metrics/#magic-number) > 1.1 -P0
1. [Gross margin](/handbook/finance/financial-planning-and-analysis/#long-term-profitability-targets) > 0.85 -P0
1. [Average days of sales outstanding](/handbook/finance/operating-metrics/#days-sales-outstanding-dso) < 45 -P0
1. [Average days to close](/handbook/finance/accounting/#month-end-review--close) (out of scope for Q2)
1. [Runway > 12 months](/handbook/finance/operating-metrics/#cash-burn-average-cash-burn-and-runway) -P1
1. [New hire location factor](/handbook/people-operations/people-operations-metrics/#low-location-factor-reporting) < 0.73  -P0
1. [ARR by annual cohort](/handbook/finance/operating-metrics/#arr-cohort) - P0
1. [Reasons for churn](/handbook/customer-success/vision/#measurement-and-kpis) -P0
1. [Reasons for net expansion](/handbook/customer-success/vision/#measurement-and-kpis) -P0
1. [Refunds processed as % of orders](/handbook/support/workflows/services/gitlab_com/verify_subscription_plan.html#refunds-processed-as--of-orders) -P1

## Product KPIs

1. SMAU (Pings)
1. [MAU](/handbook/product/metrics/#monthly-active-user-mau) (GitLab.com + Pings) - P1
1. Stage Monthly Active Instances (SMAI) (Pings) - P2
1. Monthly Active Instances (MAI) (Pings) - P2
1. [Sessions on release post](/handbook/finance/operating-metrics/#sessions-release-post)  (Google Analytics) - P2
1. Installation churn (Pings) - P2
1. User churn (Pings) - P2
1. Lost Instances (Pings) - P2
1. [Acquisition](/handbook/product/metrics/#acquistion) (out of scope for Q2 - Snowplow)
1. [Activation](/handbook/product/metrics/#activation) (out of scope for Q2 - Snowplow)
1. [Adoption](/handbook/product/metrics/#adoption) (out of scope for Q2 - Snowplow)
1. [Upsell](/handbook/product/metrics/#upsell) (out of scope for Q2 - Snowplow)
1. [CI Pipeline Minute Consumption](/handbook/product/metrics/#ci-pipeline-minute-consumption) - P1
1. [Retention](/handbook/product/metrics/#retention) (out of scope for Q2 - Snowplow)
1. Net Promotor Score / Customer Satisfaction with the product (unclear definition)
1. [New hire location factor](/handbook/people-operations/people-operations-metrics/#low-location-factor-reporting) < 0.72 - P1
1. Signups (GitLab.com) - P2
1. Onboarding completion rate (Snowplow) - P2
1. Comments (GitLab.com) - P2
1. Accepting Merge Requests issue growth (GitLab.com) - P2
1. Number of customer discovery conversations per product manager = 1 per month (out of scope for Q2)
1. [System Usability Score](/handbook/engineering/ux/#system-usability-score) (out of scope for Q2)
1. [Provably successful direction items](/handbook/product/metrics/#provably-successful-direction-items) - P2

## Engineering KPIs

1. Merge Requests per release per engineer in product development > 10 (GitLab.com) - P2
1. [Days to merge](https://docs.google.com/document/d/1NNne33rOtkrogqWRzdQZ4U3kiZdc2PC6B44WCpmQpNc/edit#heading=h.pk626p1rzft2) < 7 - New
1. Uptime GitLab.com > 99.95% (out of scope for Q2 - Pingdom?)
1. Performance GitLab.com (out of scope for Q2 - Pingdom? Pagerduty? Not sure)
1. [Priority Support Service Level Agreement (SLA)](/handbook/support/#weekly-metrics-in-brief) - P2
1. [Support CSAT](/support/#customer-satisfaction) - P2
1. Support cost vs. recurring revenue (Netsuite over Zuora) - P0
1. Days to fix S1 security issues (GitLab.com) - P1
1. Days to fix S2 security issues (GitLab.com) - P1
1. Days to fix S3 security issues (GitLab.com) - P1
1. GitLab.com infrastructure cost per MAU (Netsuite over GitLab.com) - P0
1. [ARR](/handbook/finance/operating-metrics/#annual-recurring-revenue-arr) per support rep > $1.175M (Zuora over Bamboo) - P0
1. [New hire location factor < 0.58](/handbook/hiring/charts/engineering-function/) - P1
1. Public Cloud Spend (Netsuite?) - P0

## Alliances KPIs

1. Active users per hosting platform: Total, AWS, Azure, GCP, IBM, Red Hat, Digital Ocean, etc (out of scope for Q2)
1. Active installations per hosting platform: Total, AWS, Azure, GCP, Red Hat, Digital Ocean, Unknown (out of scope for Q2)
1. Product Downloads: Updates & Initial per distribution method: Omnibus, Cloud native helm chart, Source, etc (out of scope for Q2)
1. Acquisition velocity: [Acquire 3 teams per quarter](/handbook/alliances/#acquisitions) for less than $2m in total. (out of scope for Q2)
1. Acquisition success: 70% of acquisitions ship the majority of their old product functionality as part of GitLab within 3 months after acquisition. (out of scope for Q2)
1. [New hire location factor](/handbook/people-operations/global-compensation/#location-factor) < 0.82 -P0

## GitLab Metrics

We share a spreadsheet with investors called "GitLab Metrics", these can be found in the [dashboard with the same name](https://app.periscopedata.com/app/gitlab/409920/WIP:-GitLab-Metrics).

## All other metrics

Many metrics are important for tracking progress, but are not the top KPIs for the organization.
If a metric is listed above, it should not be listed in the below list. We do not need to maintain things in multiple places.

* Average Sales Price (ASP)
* Capital Consumption
* Cash Burn, Average Cash Burn and Runway
   * ProServe Contract Value (PCV)
   * Total Contract Value (TCV)
* Cost per MQL
* Credit
* Customers
   * Customer Segmentation
   * Customer Counts
* Customer Acquisition Cost (CAC)
* Customer Acquisition Cost (CAC) Ratio
* Downgrade
* Field efficiency ratio
* Free Cash Flow (FCF)
* GitLab.com User and Group Churn
* Licensed Users
* Life-Time Value (LTV)
* Life-Time Value to Customer Acquisition Cost Ratio (LTV:CAC)
* Lost instances
* Lost Renewal
* Magic Number
* Marketing efficiency ratio
* Marketo Qualified Lead (MQL)]Customer lifecycle](/handbook/business-ops/#customer-lifecycle)
* Monthly Active Group (MAG)
* Monthly Active User (MAU)
* New ACV / New Customers
* New ACV / New Customers by Sales Assisted
* Revenue per Licensed User (also known as ARPU)
* Sales Efficiency Ratio
* Social Response Time
* [Team Members](/handbook/people-operations/people-operations-metrics/)
