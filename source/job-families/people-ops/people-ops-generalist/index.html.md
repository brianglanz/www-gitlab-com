---
layout: job_family_page
title: "People Operations Generalist"
---
<a id="intermediate-requirements"></a>
### Responsibilities

- Assist the Chief People Officer to develop the strategic direction for People Ops, and implement the functional steps to achieve those goals.
- Develop and implement HR policies, from recruitment and pay, to diversity and employee relations.
- Work with People Ops and the executive team to establish entities or co-employers in new countries as we scale.
- Develop contracts and benefits programs for each entity
- Manage co-employer relationships and act as the main point of contact
- Partner with Legal on international employment and contractual requirements
- Create People Operations processes and strategies following the GitLab workflow, with the goal always being to make things easier from the perspective of the team members.
- Keep it efficient and DRY.
- Oversee all offboarding tasks and provide assistance when needed
- Suggest and implement improvements to People Operations, for example for performance reviews and various types of training.
- Process and manage Parental Leaves based on each entity benefit policy.
- Handle work permits/transfers for new hires and the current GitLab team, including the WBSO grant.
- Coordinate with Finance on payroll reporting.
- Process and manage the Tuition Reimbursement policy for GitLab.
- Collaborate with the HRBPs on Training Programs for Managers.
- Provide HR generalist support to HRBPs as and when needed
- Provide assistance in responding to emails in the people operations email and Slack channel

### Requirements

- The ability to work autonomously and to drive your own performance & development would be important in this role.
- Prior extensive experience in an HR or People Operations role
- Clear understanding of HR laws in one or multiple countries where GitLab is active
- Willingness to work strange hours when needed (for example, to call an embassy in a different continent)
- Excellent written and verbal communication skills
- Exceptional customer service skills
- Strong team player who can jump in and support the team on a variety of topics and tasks
- Enthusiasm for and broad experience with software tools
- Proven experience quickly learning new software tools
- Willing to work with git and GitLab whenever possible
- Willing to make People Operations as open and transparent as possible
- Proven organizational skills with high attention to detail and the ability to prioritize.
- You share our [values](/handbook/values), and work in accordance with those values
- The ability to work in a fast paced environment with strong attention to detail is essential.
- High sense of urgency
- Bachelors degree or 2 years related experience
- Experience at a growth-stage tech company is preferred
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).

## Levels

### People Operations Specialist

The People Ops Specialist title is the equivalent of Junior People Ops Generalist.

A junior team member in this role will be able to perform the majority of this role with a moderate level of guidance from the People Ops Director.  They will be more focused on execution of the above tasks rather than the strategy behind those decisions. However, it is the intention that this role will learn to make those independent choices and will determine ways to increase the efficiency of the tasks needed in this role.  Targeted areas of focus for the team member in this role will include:

- Coordinate all offboarding tasks and provide assistance when needed.
- Provide assistance in responding to emails in the people operations email and Slack channel to ensure extraordinary customer service to employees and leaders at GitLab.
- Process and manage Parental Leaves based on each entity benefit policy.
- Handle work permits/transfers for new hires and the current GitLab team, including the WBSO grant.
- Coordinate with Finance on payroll reporting.
- Process and manage the Tuition Reimbursement policy for GitLab.
- Order Supplies (Laptop, office equipment, business cards, etc.)
- Coordinate birthday and anniversary notifications
- Responsible for providing support for company wide use of Google, Slack, and Zoom (configuration, permissions, troubleshooting)
- Manage and Moderate Functional and Company team meetings
- Work closely with People Operations and broader People team to implement specific People Operations processes and transactions
- Maintain appropriate level of process, program, and policy knowledge in order to assist team members.
- Complete ad-hoc projects, reporting, and tasks.
- Proactively identify process inefficiencies and inconsistencies and collaborate towards an improved and more productive process that improves the employee and/or manager’s experience.
- Assist with other People Operation activities as needed.

### Senior People Operations Generalist

A Senior level team member in this role will be expected to be strategic and operational. They will need to identify key areas for improvement or scale and to suggest solutions. They will also be a mentor to other members on the People Operations and Recruiting teams. They will understand employment law and best practices and will be able to translate that knowledge into scalable solutions for GitLab.  Some key areas of focus for the Senior People Ops Generalist will include:

- Assist the Chief People Officer to develop the strategic direction for People Ops and implement the functional steps to achieve those goals.
- Develop and implement HR policies, from recruitment and pay, to diversity and employee relations.
- Work with People Ops and the executive team to establish entities or co-employers in new countries as we scale.
- Develop contracts and benefits programs for each entity.
- Manage co-employer relationships and act as the main point of contact.
- Partner with Legal on international employment and contractual requirements.
- Create People Operations processes and strategies following the GitLab workflow, with the goal always being to make things easier from the perspective of the team members.
- Collaborate with the HRBPs on Training Programs for Managers.
- Provide HR generalist support to HRBPs as and when needed.
- Provide assistance in responding to emails in the people operations email and Slack channel.

### Manager People Operations


- Lead and coach a team People Operation Specialists to scale with the dynamic demands of a rapidly growing world-wide technology company
- Onboard, mentor, and grow the careers of all team members
- Partner with stakeholders to develop strategies, priorities and process improvements according to fluctuating business demand
- Partner with recruiting and other internal functional group leaders to ensure the overall team understands business needs and meets KPI's
- Provide coaching to improve performance of team members and drive accountability
- Deep understanding of the business and ability to align people operations support based on priority and impact.
- Consistent and effective communication to all stakeholders to ensure alignment and clear understanding of priorities
- Drive People Operations processes and strategies following the GitLab workflow, with the goal always being to make things easier from the perspective of the team members.
- Oversee the co-employer relationships and act as the main point of contact.
- Partner with Finance and Legal to establish entities or co-employers in new countries as we scale.